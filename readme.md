# Horoscop

Plausible fake horoscope generator in Romanian. [See it live.][live]

![Cover of Horoscop.](screenshot.png)

## Usage

Install:

    yarn

Start the server and watch for changes:

    yarn start

Built it:

    yarn build

## License

ISC

[live]: http://nechifor.net/horoscop
